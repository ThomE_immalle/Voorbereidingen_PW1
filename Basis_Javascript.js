﻿function Oefening1() {
    var array = ["Henk", "Ward", "Bert"];
    function zegWaarden() {
        console.log(array);
    };
    zegWaarden();
}

function Oefening2() {
    function zegHalloPersoon(naam) {
        return "Hallo " + naam;
    }

    console.log(zegHalloPersoon("Henk"));
    console.log(zegHalloPersoon("Wardje"));
}

function Oefening3() {
    function zegGetal(getal) {
        if (getal < 100) {
            return "Kleiner dan 100";
        } else {
            return "Groter dan 100";
        }
    };

    console.log(zegGetal(500));
    console.log(zegGetal(25));
}

function Oefening4() {
    function namenArray() {
        var namen = ["Henk", "Bert", "Frank", "Freddy"];
        return namen.splice(1, namen.length-2);
    };
    console.log(namenArray());
}

function Oefening5() {
    var temperaturen = "25.0; 23.4; 45.8; 34.30";
    var temperaturenSplit = temperaturen.split(";")
    console.log(temperaturenSplit);
}

function Oefening6() {
    var namen = ["Henk", "Bert", "Juan", "Rodriguez"];
    var namenSamen = namen.join("\n");
    console.log(namenSamen);
}

console.log(Oefening1());
console.log(" ");
console.log(Oefening2());
console.log(" ");
console.log(Oefening3());
console.log(" ");
console.log(Oefening4());
console.log(" ");
console.log(Oefening5());
console.log(" ");
console.log(Oefening6());